create table account
(
    id           uuid,
    username     text      not null,
    email        text      not null,
    password     text      not null,
    display_name text,
    avatar_url   text,
    biography    text,
    create_at    timestamp not null,
    update_at    timestamp not null,

    constraint pk_account primary key (id),
    constraint u_username unique (username),
    constraint u_email unique (email)
);