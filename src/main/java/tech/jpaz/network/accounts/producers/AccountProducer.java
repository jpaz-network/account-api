package tech.jpaz.network.accounts.producers;

import tech.jpaz.network.accounts.entities.Account;

public interface AccountProducer {

    void onCreate(Account account);

}
