package tech.jpaz.network.accounts.producers.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import tech.jpaz.network.accounts.entities.Account;
import tech.jpaz.network.accounts.producers.AccountProducer;
import tech.jpaz.network.broker.messages.AccountCreateMessage;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccountProducerImpl implements AccountProducer {

    private final JmsTemplate jmsTemplate;

    @Override
    public void onCreate(Account account) {
        var accountCreateMessage = new AccountCreateMessage(
                account.getId(),
                account.getUsername(),
                account.getEmail(),
                account.getDisplayName(),
                account.getAvatarUrl()
        );

        log.info("sending account create message: " + accountCreateMessage);

        this.jmsTemplate.convertAndSend("account.create", accountCreateMessage);
    }
}
