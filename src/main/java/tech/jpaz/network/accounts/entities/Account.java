package tech.jpaz.network.accounts.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tech.jpaz.network.accounts.requests.AccountRequest;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account", uniqueConstraints = {
        @UniqueConstraint(name = "username", columnNames = "username"),
        @UniqueConstraint(name = "email", columnNames = "email")
})
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @NotEmpty
    private String username;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    private String displayName;

    private String avatarUrl;

    private String biography;

    @NotNull
    private LocalDateTime createAt;

    @NotNull
    private LocalDateTime updateAt;

    public Account(AccountRequest request) {
        this.username = request.username();
        this.email = request.email();
        this.password = request.password();
        this.displayName = request.displayName();
        this.biography = request.biography();
        var now = LocalDateTime.now();
        this.createAt = now;
        this.updateAt = now;
    }
}
