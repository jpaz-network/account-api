package tech.jpaz.network.accounts.responses;

import tech.jpaz.network.accounts.entities.Account;

public record AccountResponse(
        String username,
        String email,
        String displayName,
        String avatarUrl,
        String biography
) {
    public AccountResponse(Account account) {
        this(
                account.getUsername(),
                account.getEmail(),
                account.getDisplayName(),
                account.getAvatarUrl(),
                account.getBiography()
        );
    }
}
