package tech.jpaz.network.accounts.services.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.jpaz.network.accounts.entities.Account;
import tech.jpaz.network.accounts.producers.AccountProducer;
import tech.jpaz.network.accounts.repositories.AccountRepository;
import tech.jpaz.network.accounts.requests.AccountRequest;
import tech.jpaz.network.accounts.responses.AccountResponse;
import tech.jpaz.network.accounts.services.AccountService;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountProducer accountProducer;
    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public AccountResponse create(AccountRequest request) {
        var account = this.accountRepository.save(new Account(request));

        this.accountProducer.onCreate(account);

        return new AccountResponse(account);
    }

    @Override
    @Transactional
    public AccountResponse findByUsername(String username) {
        return this.accountRepository.findByUsername(username)
                .map(AccountResponse::new)
                .orElse(null);
    }
}
