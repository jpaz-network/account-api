package tech.jpaz.network.accounts.services;

import tech.jpaz.network.accounts.requests.AccountRequest;
import tech.jpaz.network.accounts.responses.AccountResponse;

public interface AccountService {

    AccountResponse create(AccountRequest request);

    AccountResponse findByUsername(String username);

}
