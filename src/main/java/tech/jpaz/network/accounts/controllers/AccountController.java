package tech.jpaz.network.accounts.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import tech.jpaz.network.accounts.requests.AccountRequest;
import tech.jpaz.network.accounts.responses.AccountResponse;
import tech.jpaz.network.accounts.services.AccountService;

@Controller
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @MutationMapping
    public AccountResponse signup(@Argument AccountRequest request) {
        return this.accountService.create(request);
    }

    @QueryMapping
    public AccountResponse findByUsername(@Argument String username) {
        return this.accountService.findByUsername(username);
    }

}
