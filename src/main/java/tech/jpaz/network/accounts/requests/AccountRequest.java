package tech.jpaz.network.accounts.requests;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;

public record AccountRequest(
        @NotEmpty
        String username,
        @NotEmpty
        @Email
        String email,
        @NotEmpty
        String password,
        String displayName,
        String biography
) {
}
